const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');

const generateAccessToken = (id, username) => {
    const payload = {
        id,
        username
    };

    return jwt.sign(payload, secret, {expiresIn: "24h"});
};

class usersController {

    async changePassword(req, res) {
        try {
            const {
                oldPassword,
                newPassword
            } = req.body;
            const token = req.headers.authorization.split(" ")[1];
            const hashPassword = bcrypt.hashSync(newPassword, 7);

            const decodedData = jwt.verify(token, secret);
            const username = decodedData.username;

            const updatedUser = await User.updateOne({username}, {
                $set: {
                    password: hashPassword
                }
            });

            res.status(200).json({updatedUser, message: 'Change success'});
        } catch (e) {
            res.status(400).json({message: 'Changing  error'});
        }
    }

    async getInfo(req, res) {
        try {
            const token = req.headers.authorization.split(" ")[1];
            const decodedData = jwt.verify(token, secret);
            const id = decodedData.id;

            const user = await User.findById(id)

            res.status(200).json({user: user});
        } catch (e) {
            res.status(400).json({message: 'Get info error'});
        }
    }

    async deleteProfile(req, res) {
        try {
            const token = req.headers.authorization.split(" ")[1];
            const decodedData = jwt.verify(token, secret);
            const id = decodedData.id;

            const user = await User.findByIdAndDelete(id)

            res.status(200).json({user, message: 'Delete success'});
        } catch (e) {
            res.status(400).json({message: 'Get info error'});
        }
    }
}

module.exports = new usersController();
