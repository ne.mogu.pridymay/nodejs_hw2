const {Schema, model} = require('mongoose');


const Note = new Schema({
    _userId: {type: String, unique: false, required: true},
    text: {type: String, required: true},
    completed: {type: Boolean, default: false}
})

module.exports = model('Note', Note);
