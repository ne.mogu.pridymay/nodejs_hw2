const Router = require('express');
const router = new Router();
const controller = require('../controllers/userController');
const authMiddleware = require('../middleware/authorizationMiddleware');



router.patch('/me', authMiddleware, controller.changePassword)

router.get('/me', authMiddleware, controller.getInfo)

router.delete('/me', authMiddleware, controller.deleteProfile)




module.exports = router;
