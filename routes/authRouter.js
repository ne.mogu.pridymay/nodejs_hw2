const Router = require('express');
const router = new Router();
const controller = require('../controllers/authController');
const authMiddleware = require('../middleware/authorizationMiddleware');

router.post('/register', controller.registration);

router.post('/login', controller.login);


module.exports = router;
