const Router = require('express');

const router = new Router();

const authRouter = require('./authRouter');
const userRouter = require('./userRouter');
const noteRouter = require('./noteRouter');

router.use('/auth', authRouter)
router.use('/users', userRouter)
router.use('/notes', noteRouter)


module.exports = router;
